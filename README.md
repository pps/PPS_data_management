# PPS Data Management Example

This gitlab repository contains an example R project to set up a reproducible analysis as detailed in PPS Data Management Plan.

To download it on your computer:

1. Make an empty directory.

2. Set it as working directory in R.

3. Paste the following command in your terminal:

``` r
source("https://git.wageningenur.nl/pps/PPS_data_management/-/raw/master/download_project.R")
```

